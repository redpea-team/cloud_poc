package com.redpea.container.api.simple.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;


/**
 *
 * Controller to return account conversion results
 */
@RestController
public class RequestsController {
	

	@RequestMapping(value = "/cloud/mysql/V2", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public Global_Values account_Convert_V2(@RequestBody Global_Values data) throws JsonProcessingException {

		System.out.println("global session data is : "+data.getUrl());
		
		Global_Values session = new Global_Values();
		session.setUrl(data.getUrl());
		session.setDriver(data.getDriver());
		session.setUser(data.getUser());
		session.setPassword(data.getPassword());
		
		/* test JDBC */
		
		try {

			//Class.forName(data.getDriver());

			Connection connection = DriverManager.getConnection(data.getUrl(), data.getUser(),
					data.getPassword());
			String sql = "show tables";

			PreparedStatement pstmt = connection.prepareStatement(sql);

			if (pstmt.execute()) {

				int size = 0;
				ResultSet rs = pstmt.getResultSet();

				while (rs.next()) {
					size = rs.getRow();
					System.out.println(rs.getString(1));
				}
				System.out.printf("Row Count is %d", size);

			} else {
				System.out.println("Execute failed!");
			}

			if (connection != null)
				connection.close();

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}

		return session;
	}

}
